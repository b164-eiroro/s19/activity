console.log("Hello");
// cube number using template literals
const num1 = 2;
const num2 = 3;
console.log(`The cube of ${num1} is ${num1**num2}.`)

// address using array destructure and template literals
const address = ["Avocado St.", "Citizens Village", "Polomolok", "South Cotabato", 9504 ]
const [street, village, municipality, region, zip] = address;

console.log(`I live at ${street} ${village} ${municipality} ${region} ${zip}`)

// animals using array destructure
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {name, species, weight, measurement} = animal;
console.log(`${name} was  a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`)

//  loop using arrow function and implicit return statement
const numbers = [1, 2, 3, 4, 5]
numbers.forEach((number) => {
	console.log(number)
})

// using redduce array method  and arrow function to sum the numbers
const reduceNumber = 0
const sum = numbers.reduce((previousValue, currentValue) =>previousValue + currentValue,reduceNumber);

console.log(sum)

//  using class and a constuctor
class Dog {
	constructor(name, age, breed) {
		this.name = name
		this.age = age
		this.breed = breed
	}
}

const myDog = new Dog();
myDog.name = "Summer"
myDog.age = "6 months"
myDog.breed = "Golden Retreiver"
console.log(myDog)